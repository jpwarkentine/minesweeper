import game from "./MINE_main.js";

const rootDiv = document.getElementById("root");

for (let i = 1; i <= game.size; i++) {
    const newRow = document.createElement("tr");

    for (let j = 1; j <= game.size; j++) {
        const newSquare = document.createElement("td");
        const squareContents = game.findSquare(j, i)?.render();
        newSquare.textContent = squareContents;
        newSquare.setAttribute("style", "padding: 25px; border-style: solid;");
        newSquare.onclick = () => clickFunction(newSquare, j, i);
        newRow.appendChild(newSquare);
    }

    rootDiv.appendChild(newRow);
}


function clickFunction(squareEl, j, i) {
    const clickedSquare = game.clickSquare(j, i);
    squareEl.textContent = clickedSquare.render();
}
