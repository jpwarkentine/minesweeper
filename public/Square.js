class Square {
    constructor(isMine, x, y, numAdjMines = 0) {
        this.id = crypto.randomUUID();
        this.isMine = isMine;
        this.x = x;
        this.y = y;
        this.isActivated = false;
        this.renderValue = "?";

        this.metadata = {};
        this.metadata.numAdjMines = numAdjMines;
    }

    activate() {
        this.isActivated = true;
        this.setRenderValue();
        this.render();
        const isExplosion = this.isMine;
        return isExplosion;
    }

    setRenderValue() {
        if (this.isActivated) {
            if (this.isMine) {
                this.renderValue = "X";
            } else {
                this.renderValue = this.metadata.numAdjMines;
            }
        } else {
            this.renderValue = "?";
        }

        return this.renderValue;
    }

    render() {
        return this.renderValue;
    }
}

export default Square;
