import Minefield from "./Minefield.js";

class MinefieldInterface {
    constructor(size, numMines) {
        this.minefield = new Minefield(size, numMines);
        this.size = size;
    }

    firstClick(x, y) {
        this.minefield.populateMinefield();
        this.minefield.generateSquares();
        this.minefield.calcAdjMines();

        this.clickSquare(x, y);
    }

    clickSquare(x, y) {
        const clickedSquare = this.findSquare(x, y);
        clickedSquare.activate();
        return clickedSquare;
    }

    findSquare(x, y) {
        return this.minefield.squares.filter(sq => sq.x === x && sq.y === y)[0];
    }
}

export default MinefieldInterface;
