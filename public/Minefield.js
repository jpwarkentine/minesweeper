// note that we could make an array of rows, each row being an array of squares, and use the indexes as coordinates

import Square from "./Square.js";

class Minefield {
    constructor(size = 25, numMines = size / 2) {
        this.size = size;
        this.numMines = numMines;
        this.metadata = {};
        this.metadata.mineHashes = [];
        this.metadata.mines = [];
        this.squares = [];
    }

    generateSquares() {
        for (let x = 1; x <= this.size; x++) {
            for (let y = 1; y <= this.size; y++) {
                const isMine = this.metadata.mines.some(m => m.y === y && m.x === x);
                const newSquare = new Square(isMine, x, y);
                this.squares.push(newSquare);
            }
        }
    }

    calcAdjMines() {
        this.squares.forEach((square, index, squares) => {
            if (square.isMine) {
                const xRangeMin = square.x - 1;
                const xRangeMax = square.x + 1;
                const yRangeMin = square.y - 1;
                const yRangeMax = square.y + 1;

                const adjSquares = squares.filter(sq => {
                    return xRangeMax >= sq.x && sq.x >= xRangeMin && yRangeMax >= sq.y && sq.y >= yRangeMin;
                });

                adjSquares.forEach(s => {
                    s.metadata.numAdjMines++;
                });
            }
        });
    }

    populateMinefield() {
        for (let i = 0; i < this.numMines; i++) {
            const mine = this.populateSingleMine();
            this.metadata.mines.push(mine);
            this.metadata.mineHashes.push(this.hashCoordinates(mine));
        }
    }

    populateSingleMine() {
        const coordinates = this.randomCoordinates();
        if (this.metadata.mines.some(m => m.y === coordinates.y && m.x === coordinates.x)) { // ensures the mine placement is unique via recursion
            return this.populateSingleMine();
        }

        return coordinates;
    }

    randomCoordinates() {
        const x = Math.ceil(Math.random() * this.size);
        const y = Math.ceil(Math.random() * this.size);

        return { x, y };
    }

    hashCoordinates(xORhash, y) {
        if (xORhash.x && xORhash.y) {
            return this.szudzikPair(xORhash.x, xORhash.y)
        }

        if (xORhash && !y) { // if only hash is passed, dehash
            return this.szudzikUnpair(xORhash);
        }

        // otherwise hash
        return this.szudzikPair(xORhash, y);
    }

    szudzikPair(x, y) {
        // https://www.vertexfragment.com/ramblings/cantor-szudzik-pairing-functions/
        return (x >= y ? (x * x) + x + y : (y * y) + x);
    }

    szudzikUnpair(z) {
        // http://szudzik.com/ElegantPairing.pdf
        // https://stackoverflow.com/questions/53802655/how-to-write-szudziks-function-in-java
        const b = Math.floor(Math.sqrt(z));
        const a = z - (b * b);
        return a < b ? { x: a, y: b } : { x: b, y: (a - b) };
    }
}

export default Minefield;
