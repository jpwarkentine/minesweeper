const express = require('express');
const app = express();
const PORT = 3002;

app.get('/', (req, res) => {
    res.sendFile('/Users/jpwarkentine/Repos/minesweeper/index.html')                   // GET Route for homepage
});

app.use(express.static('public'));

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
});